# use-dataclasses

- Follow tutorial --- [This Is Why Python Data Classes Are Awesome](https://www.youtube.com/watch?v=CvQ7e6yUtnw)

## 📝 Notes

`slots=True` would save up performance, but it won't work with **multiple inheritance**.

When to use or not to use dataclasses?

See https://www.reddit.com/r/Python/comments/ycn5ae/any_reason_not_to_use_dataclasses_everywhere/.
